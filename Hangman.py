def draw_hangman(remaining_lives):
    if remaining_lives == 11:
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("            ")
    elif remaining_lives == 10:
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("/           ")
    elif remaining_lives == 9:
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("            ")
        print("/ \         ")
    elif remaining_lives == 8:
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print("/ \         ")
    elif remaining_lives == 7:
        print(" |--------  ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print("/ \         ")
    elif remaining_lives == 6:
        print(" |--------  ")
        print(" |       |  ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print("/ \         ")
    elif remaining_lives == 5:
        print(" |--------  ")
        print(" |       |  ")
        print(" |       O  ")
        print(" |          ")
        print(" |          ")
        print(" |          ")
        print("/ \         ")
    elif remaining_lives == 4:
        print(" |--------  ")
        print(" |       |  ")
        print(" |       O  ")
        print(" |       |  ")
        print(" |          ")
        print(" |          ")
        print("/ \         ")
    elif remaining_lives == 3:
        print(" |--------  ")
        print(" |       |  ")
        print(" |       O  ")
        print(" |       |\ ")
        print(" |          ")
        print(" |          ")
        print("/ \         ")
    elif remaining_lives == 2:
        print(" |--------  ")
        print(" |       |  ")
        print(" |       O  ")
        print(" |      /|\ ")
        print(" |          ")
        print(" |          ")
        print("/ \         ")
    elif remaining_lives == 1:
        print(" |--------  ")
        print(" |       |  ")
        print(" |       O  ")
        print(" |      /|\ ")
        print(" |        \ ")
        print(" |          ")
        print("/ \         ")
    elif remaining_lives == 0:
        print(" |--------  ")
        print(" |       |  ")
        print(" |       O  ")
        print(" |      /|\ ")
        print(" |      / \ ")
        print(" |          ")
        print("/ \         ")


def clear_console():
    print ("\n" * 35)


def check_if_guess_is_correct(guess):
    no_of_letters_found = answer.count(guess)
    if (no_of_letters_found > 0):
        print(f"Correct! Letter '{guess}' occurs in the word")
        return True
    else:
        print(f"Wrong! Letter '{guess}' is not there")
        return False

    
def reveal_hidden_letter(guess):
    index = 0
    for character in answer:
        if answer[index] == guess:
            hidden_answer[index] = guess
        index += 1


remaining_lives = 11
answer = input("Type a word to be guessed by another user: ")
clear_console()
hidden_answer = []
for n in range(len(answer)):
    hidden_answer.append("_")


while remaining_lives > 0:
    guessed_letters = ''.join(hidden_answer)
    if answer == guessed_letters:
        print("Congratulations, you won!")
        break
    else:
        guess = input("Guess a letter: ")
        if check_if_guess_is_correct(guess) == True:
            reveal_hidden_letter(guess)
        else:
            remaining_lives -= 1
        print(f"You have {remaining_lives} lives left.\n")
        print(hidden_answer)
        draw_hangman(remaining_lives)

print("GAME OVER!")
